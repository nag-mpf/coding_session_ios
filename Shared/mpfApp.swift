//
//  mpfApp.swift
//  Shared
//
//  Created by Nagaraj Alagusundaram on 8/4/2022.
//

import SwiftUI

@main
struct mpfApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
